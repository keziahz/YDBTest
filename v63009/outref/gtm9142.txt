# Starting from V6.3-009 MUPIP REORG now recognizes the commands -NOCOALESCE -NOSPLIT and -NOSWAP
Files Created in ##TEST_PATH##:
Using: ##SOURCE_PATH##/mumps -run GDE
mumps.gld
Using: ##SOURCE_PATH##/mupip
mumps.dat
# Testing to see if the following qualifiers are recognized
# Using NOSWAP
Fill Factor:: Index blocks 100%: Data blocks 100%
%GTM-W-NOSELECT, None of the selected variables exist -- halting
# Using NOCOALESCE
Fill Factor:: Index blocks 100%: Data blocks 100%
%GTM-W-NOSELECT, None of the selected variables exist -- halting
# Using NOSPLIT
Fill Factor:: Index blocks 100%: Data blocks 100%
%GTM-W-NOSELECT, None of the selected variables exist -- halting
/usr/library/V63009/dbg/mupip
/usr/library/V63009/dbg/mupip integ -REG *
No errors detected by integ.
