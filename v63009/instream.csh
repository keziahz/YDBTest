#!/usr/local/bin/tcsh -f

#################################################################
#								#
# Copyright (c) 2020 YottaDB LLC and/or its subsidiaries.	#
# All rights reserved.						#
#								#
#	This source code contains the intellectual property	#
#	of its copyright holder(s), and is made available	#
#	under a license.  If you do not know the terms of	#
#	the license, please stop and do not read further.	#
#								#
#################################################################
#
#----------------------------------------------------------------------------------------------------------------------------------------------------------------
# List of subtests of the form "subtestname [author] description"
#-------------------------------------------------------------------------------------
# gtm9142               [mw]            Test that MUPIP REORG recognizes the -NOCOALESCE, -NOSPLIT and -NOSWAP qualifiers
# gtm8203               [mw]            Test to show that MUPIP REORG -TRUNCATE now supports -KEEP
# gtm9145               [mw]            Test that the code line length has been increased for ^%RI and ^%RO
# gtm8901		[mw]		Test the new quailfer -GVPATFILE for MUPIP JOURNAL -EXTRACT can extract patterns from a file
# gtm8706		[mw]		Test the new qualifer -STOPRECEIVERFILTER in MUPIP REPLICATE -RECEIVER
#----------------------------------------------------------------------------------------------------------------------------------------------------------------


echo "v63009 test starts..."

# List the subtests seperated by sspaces under the appropriate environment variable name
setenv subtest_list_common	""
setenv subtest_list_non_replic "gtm9142 gtm8203 gtm9145 gtm8901"
setenv subtest_list_replic	"gtm8706"

if ($?test_replic == 1) then
	setenv subtest_list "$subtest_list_common $subtest_list_replic"
else
	setenv subtest_list "$subtest_list_common $subtest_list_non_replic"
endif

setenv subtest_exclude_list ""
if("pro" == "$tst_image") then
	setenv subtest_exclude_list "$subtest_exclude_list "
endif

# Use $subtest_exclude_list to remove subtests that are to be disabled on a particular host or OS
if ("pro" == "$tst_image") then
	setenv subtest_exclude_list "$subtest_exclude_list "
endif

# Submit the list of subtests
$gtm_tst/com/submit_subtest.csh

echo "v63009 test DONE."
